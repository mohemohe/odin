﻿using Livet;
using Livet.Commands;
using Livet.Messaging.Windows;
using System.IO;
using System.Reflection;

namespace OdinWPF.ViewModels
{
    public class SettingsWindowViewModel : ViewModel
    {
        public void Initialize()
        {
            GetVersion();
        }

        private void GetVersion()
        {
            var location = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            OdinVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            SleipnirVersion = Assembly.LoadFrom(Path.Combine(location, @"Sleipnir.dll")).GetName().Version.ToString();
        }

        #region OdinVersion変更通知プロパティ

        private string _OdinVersion;

        public string OdinVersion
        {
            get
            { return _OdinVersion; }
            set
            {
                if (_OdinVersion == value)
                    return;
                _OdinVersion = value;
                RaisePropertyChanged();
            }
        }

        #endregion OdinVersion変更通知プロパティ

        #region SleipnirVersion変更通知プロパティ

        private string _SleipnirVersion;

        public string SleipnirVersion
        {
            get
            { return _SleipnirVersion; }
            set
            {
                if (_SleipnirVersion == value)
                    return;
                _SleipnirVersion = value;
                RaisePropertyChanged();
            }
        }

        #endregion SleipnirVersion変更通知プロパティ

        #region OKCommand

        private ViewModelCommand _OKCommand;

        public ViewModelCommand OKCommand
        {
            get
            {
                if (_OKCommand == null)
                {
                    _OKCommand = new ViewModelCommand(OK);
                }
                return _OKCommand;
            }
        }

        public void OK()
        {
            Apply();
            Cancel();
        }

        #endregion OKCommand

        #region CancelCommand

        private ViewModelCommand _CancelCommand;

        public ViewModelCommand CancelCommand
        {
            get
            {
                if (_CancelCommand == null)
                {
                    _CancelCommand = new ViewModelCommand(Cancel);
                }
                return _CancelCommand;
            }
        }

        public void Cancel()
        {
            Messenger.Raise(new WindowActionMessage(WindowAction.Close, "WindowMessage"));
        }

        #endregion CancelCommand

        #region ApplyCommand

        private ViewModelCommand _ApplyCommand;

        public ViewModelCommand ApplyCommand
        {
            get
            {
                if (_ApplyCommand == null)
                {
                    _ApplyCommand = new ViewModelCommand(Apply);
                }
                return _ApplyCommand;
            }
        }

        public void Apply()
        {
            // TODO: 保存
        }

        #endregion ApplyCommand
    }
}