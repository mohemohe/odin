﻿using Livet;
using Livet.Commands;
using Livet.Messaging;
using Livet.Messaging.IO;
using OdinWPF.Helpers;
using OdinWPF.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Media.Imaging;

namespace OdinWPF.ViewModels
{
    public class MainWindowViewModel : ViewModel
    {
        /* コマンド、プロパティの定義にはそれぞれ
         *
         *  lvcom   : ViewModelCommand
         *  lvcomn  : ViewModelCommand(CanExecute無)
         *  llcom   : ListenerCommand(パラメータ有のコマンド)
         *  llcomn  : ListenerCommand(パラメータ有のコマンド・CanExecute無)
         *  lprop   : 変更通知プロパティ(.NET4.5ではlpropn)
         *
         * を使用してください。
         *
         * Modelが十分にリッチであるならコマンドにこだわる必要はありません。
         * View側のコードビハインドを使用しないMVVMパターンの実装を行う場合でも、ViewModelにメソッドを定義し、
         * LivetCallMethodActionなどから直接メソッドを呼び出してください。
         *
         * ViewModelのコマンドを呼び出せるLivetのすべてのビヘイビア・トリガー・アクションは
         * 同様に直接ViewModelのメソッドを呼び出し可能です。
         */

        /* ViewModelからViewを操作したい場合は、View側のコードビハインド無で処理を行いたい場合は
         * Messengerプロパティからメッセージ(各種InteractionMessage)を発信する事を検討してください。
         */

        /* Modelからの変更通知などの各種イベントを受け取る場合は、PropertyChangedEventListenerや
         * CollectionChangedEventListenerを使うと便利です。各種ListenerはViewModelに定義されている
         * CompositeDisposableプロパティ(LivetCompositeDisposable型)に格納しておく事でイベント解放を容易に行えます。
         *
         * ReactiveExtensionsなどを併用する場合は、ReactiveExtensionsのCompositeDisposableを
         * ViewModelのCompositeDisposableプロパティに格納しておくのを推奨します。
         *
         * LivetのWindowテンプレートではViewのウィンドウが閉じる際にDataContextDisposeActionが動作するようになっており、
         * ViewModelのDisposeが呼ばれCompositeDisposableプロパティに格納されたすべてのIDisposable型のインスタンスが解放されます。
         *
         * ViewModelを使いまわしたい時などは、ViewからDataContextDisposeActionを取り除くか、発動のタイミングをずらす事で対応可能です。
         */

        /* UIDispatcherを操作する場合は、DispatcherHelperのメソッドを操作してください。
         * UIDispatcher自体はApp.xaml.csでインスタンスを確保してあります。
         *
         * LivetのViewModelではプロパティ変更通知(RaisePropertyChanged)やDispatcherCollectionを使ったコレクション変更通知は
         * 自動的にUIDispatcher上での通知に変換されます。変更通知に際してUIDispatcherを操作する必要はありません。
         */

        public void Initialize()
        {
            StatusBarHelper.StatusBarTextOnUpdate += e =>
            {
                DispatcherHelper.UIDispatcher.BeginInvoke(
                    System.Windows.Threading.DispatcherPriority.Render,
                    new Action(() => StatusBarText = e));
            };
        }

        #region Image変更通知プロパティ

        private BitmapSource _Image;

        public BitmapSource Image
        {
            get
            { return _Image; }
            set
            {
                if (_Image == value)
                    return;
                _Image = value;
                RaisePropertyChanged();
            }
        }

        #endregion Image変更通知プロパティ

        #region Message変更通知プロパティ

        private string _Message;

        public string Message
        {
            get
            { return _Message; }
            set
            {
                if (_Message == value)
                    return;
                _Message = value;
                RaisePropertyChanged();
            }
        }

        #endregion Message変更通知プロパティ

        #region StatusBarText変更通知プロパティ

        private string _StatusBarText;

        public string StatusBarText
        {
            get
            { return _StatusBarText; }
            set
            {
                if (_StatusBarText == value)
                    return;
                _StatusBarText = value;
                RaisePropertyChanged();
            }
        }

        #endregion StatusBarText変更通知プロパティ

        #region ImageSet変更通知プロパティ

        private ImageSet _ImageSet;

        public ImageSet ImageSet
        {
            get
            { return _ImageSet; }
            set
            {
                if (_ImageSet == value)
                    return;
                _ImageSet = value;
                RaisePropertyChanged();
            }
        }

        #endregion ImageSet変更通知プロパティ

        #region FileOpenCommand

        private ListenerCommand<OpeningFileSelectionMessage> _FileOpenCommand;

        public ListenerCommand<OpeningFileSelectionMessage> FileOpenCommand
        {
            get
            {
                if (_FileOpenCommand == null)
                {
                    _FileOpenCommand = new ListenerCommand<OpeningFileSelectionMessage>(FileOpen);
                }
                return _FileOpenCommand;
            }
        }

        public void FileOpen(OpeningFileSelectionMessage parameter)
        {
            var filePath = parameter?.Response?[0];
            LoadImageSet(filePath);
        }

        #endregion FileOpenCommand

        #region DropCommand

        private ListenerCommand<IEnumerable<string>> _DropCommand;

        public ListenerCommand<IEnumerable<string>> DropCommand
        {
            get
            {
                if (_DropCommand == null)
                {
                    _DropCommand = new ListenerCommand<IEnumerable<string>>(Drop);
                }
                return _DropCommand;
            }
        }

        public void Drop(IEnumerable<string> parameter)
        {
            if (!parameter.Any())
            {
                return;
            }

            LoadImageSet(parameter.First());
        }

        #endregion DropCommand

        private async void LoadImageSet(string filePath)
        {
            if (File.Exists(filePath))
            {
                await StatusBarHelper.UpdateStatusAsync(@"読み込み中: " + filePath);
                var imageSet = new ImageSet(filePath);
                imageSet.PropertyChanged += async (sender, args) =>
                {
                    var iSet = (ImageSet)sender;
                    ImageSet = iSet;
                    Image = iSet.BitmapSource;
                    Message = iSet.Message;

                    await StatusBarHelper.UpdateStatusAsync(@"読み込み完了: " + filePath);
                };
            }
        }

        #region FileSaveCommand

        private ListenerCommand<SavingFileSelectionMessage> _FileSaveCommand;

        public ListenerCommand<SavingFileSelectionMessage> FileSaveCommand
        {
            get
            {
                if (_FileSaveCommand == null)
                {
                    _FileSaveCommand = new ListenerCommand<SavingFileSelectionMessage>(FileSave);
                }
                return _FileSaveCommand;
            }
        }

        public async void FileSave(SavingFileSelectionMessage parameter)
        {
            var filePath = parameter?.Response?[0];
            ImageSet.SetString(Message);
            await ImageSet.SaveImageAsync(filePath);
        }

        #endregion FileSaveCommand

        #region OpenSettingsWindowCommand

        private ViewModelCommand _OpenSettingsWindowCommand;

        public ViewModelCommand OpenSettingsWindowCommand
        {
            get
            {
                if (_OpenSettingsWindowCommand == null)
                {
                    _OpenSettingsWindowCommand = new ViewModelCommand(OpenSettingsWindow);
                }
                return _OpenSettingsWindowCommand;
            }
        }

        public void OpenSettingsWindow()
        {
            Messenger.Raise(new TransitionMessage(new SettingsWindowViewModel(), "OpenMessage"));
        }

        #endregion OpenSettingsWindowCommand
    }
}