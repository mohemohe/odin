﻿using Livet;
using OdinWPF.Views;
using System;
using System.Diagnostics;
using System.Windows;

namespace OdinWPF
{
    /// <summary>
    /// App.xaml の相互作用ロジック
    /// </summary>
    public partial class App : Application
    {
        public App() : base()
        {
            QuickConverter.EquationTokenizer.AddNamespace(typeof(object));
            QuickConverter.EquationTokenizer.AddNamespace(typeof(int));
            QuickConverter.EquationTokenizer.AddNamespace(typeof(string));
            QuickConverter.EquationTokenizer.AddNamespace(typeof(bool));
            QuickConverter.EquationTokenizer.AddAssembly(typeof(object).Assembly);
            QuickConverter.EquationTokenizer.AddNamespace(typeof(Thickness));
            QuickConverter.EquationTokenizer.AddNamespace(typeof(Visibility));
        }

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            DispatcherHelper.UIDispatcher = Dispatcher;
#if !DEBUG
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
#endif
        }

        //集約エラーハンドラ
        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            var window = new ExceptionWindow(e);
            window.Owner = Application.Current.MainWindow;
            window.WindowStartupLocation = WindowStartupLocation.CenterOwner;

            // true: 続行, false: 終了
            var result = window.ShowDialog();
            if (result != null)
            {
                if ((bool)result)
                {
                    Process.Start(Application.ResourceAssembly.Location);
                }

                // 遅い
                //Environment.Exit(1);

                // 終了コード1を返したい
                Process.GetCurrentProcess().Kill();
            }
        }
    }
}