﻿using System.Threading.Tasks;

namespace OdinWPF.Helpers
{
    public delegate void UpdateStatusBarText(string text);

    public static class StatusBarHelper
    {
        public static event UpdateStatusBarText StatusBarTextOnUpdate;

        private static string _StatusBarText;

        public static string StatusBarText
        {
            get
            {
                return _StatusBarText;
            }

            set
            {
                _StatusBarText = value;

                if (StatusBarTextOnUpdate != null)
                {
                    StatusBarTextOnUpdate(value);
                }
            }
        }

        public static async Task UpdateStatusAsync(string text)
        {
            await Task.Run(() => StatusBarText = text);
        }
    }
}