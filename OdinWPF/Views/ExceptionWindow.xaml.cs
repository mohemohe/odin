﻿using Mohemohe.Helpers;
using System;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media.Imaging;

namespace OdinWPF.Views
{
    /// <summary>
    /// ExceptionWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class ExceptionWindow : Window
    {
        public ExceptionWindow(UnhandledExceptionEventArgs e)
        {
            InitializeComponent();

            SetErrorIcon();

            var exception = (Exception)e.ExceptionObject;
            ExceptionTitle.Text = exception.GetType().ToString();
            Message.Text = exception.Message;
            StackTrace.Text = exception.StackTrace;
        }

        private void SetErrorIcon()
        {
            var icon = Win32Helper.GetIcon(@"user32.dll", 3).ToBitmap();
            var hBitmap = icon.GetHbitmap();
            BitmapSource source = null;

            try
            {
                source = Imaging.CreateBitmapSourceFromHBitmap(hBitmap, IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
            }
            finally
            {
                Win32Helper.DeleteObject(hBitmap);
            }

            Icon.Source = source;
        }

        private void Continue_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            this.Close();
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }
    }
}