﻿using Livet;
using Mohemohe.Helpers;
using Sleipnir;
using System;
using System.Drawing.Imaging;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using RSdecoder = Sleipnir.ReedSolomon.Decoder;
using RSencoder = Sleipnir.ReedSolomon.Encoder;

namespace OdinWPF.Models
{
    public class ImageSet : NotificationObject
    {
        /*
         * NotificationObjectはプロパティ変更通知の仕組みを実装したオブジェクトです。
         */

        public string ImagePath { get; private set; }
        public StegoData StegoData { get; set; }
        public CoreImage CoreImage { get; set; }

        public BitmapSource BitmapSource
        {
            get
            {
                if (CoreImage != null && CoreImage.Bitmap != null)
                {
                    return CoreImage.Bitmap.ToBitmapSource();
                }
                else
                {
                    return new BitmapImage();
                }
            }
        }

        public string Message { get; set; }
        public bool IsWroteMessage { get; private set; }

        public ImageSet(string imagePath)
        {
            if (string.IsNullOrEmpty(imagePath))
            {
                throw new ArgumentNullException();
            }

            new Action(async () =>
            {
                ImagePath = imagePath;
                await ReadCoreImageAsync();
                await LoadStegoDataAsync();
                GetString();
                if (!string.IsNullOrEmpty(Message))
                {
                    IsWroteMessage = true;
                }
                RaisePropertyChanged();
            })();
        }

        private async Task ReadCoreImageAsync()
        {
            CoreImage = await IO.ReadImageAsync(ImagePath);
        }

        private async Task LoadStegoDataAsync()
        {
            var blockSize = await Steganography.GetPhysicalBlockSizeAsync(CoreImage);
            if (blockSize != -1)
            {
                StegoData = await Steganography.ReadAsync(CoreImage, blockSize);
            }
            //StegoData = await Steganography.ReadAsync(CoreImage);
        }

        private void GetString()
        {
            var decoder = new RSdecoder();
            try
            {
                var message = decoder.DecodeToString(StegoData?.Data);

                var messageArray = message.ToCharArray();
                for (var i = message.Length - 2; i > 0; i--)
                {
                    if (messageArray[i] != '\0')
                    {
                        messageArray = message.Substring(0, i + 1).ToCharArray();
                        break;
                    }
                }

                var errorCount = 0;
                var regex = new Regex(@"[\p{IsHiragana}\p{IsKatakana}\p{IsCJKUnifiedIdeographs}]", RegexOptions.Compiled);
                foreach (var mChar in messageArray)
                {
                    if (!regex.IsMatch(mChar.ToString()))
                    {
                        errorCount++;
                    }
                }

                if (errorCount < message.Length * 0.5)
                {
                    Message = new string(messageArray);
                }
            }
            catch { }
        }

        public void SetString(string message)
        {
            if (string.IsNullOrEmpty(message))
            {
                return;
            }

            var encoder = new RSencoder();
            var data = encoder.EncodeString(message);

            if (StegoData == null)
            {
                StegoData = new StegoData(data);
            }
            else
            {
                StegoData.SetPayloadData(data);
            }
        }

        public async Task SaveImageAsync(string filePath)
        {
            if (filePath == null)
            {
                return;
            }

            var coreImage = await Steganography.WriteAsync(CoreImage, StegoData);

            ImageFormat imageFormat = null;
            var ext = Path.GetExtension(filePath).ToLower();
            var decoders = ImageCodecInfo.GetImageDecoders();
            foreach (var imageCodecInfo in decoders)
            {
                var iciExt = imageCodecInfo.FilenameExtension.ToLower();
                if (iciExt.Contains(ext))
                {
                    imageFormat = new ImageFormat(imageCodecInfo.FormatID);
                    break;
                }
            }

            await IO.SaveImageAsync(coreImage, filePath, imageFormat);
        }
    }
}