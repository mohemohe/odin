﻿using System;
using Sleipnir;
using System.Drawing.Imaging;
using System.IO;
using ENC = Sleipnir.ReedSolomon.Encoder;

namespace Odin.TestFlight
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var enc = new ENC();

            var inDir = @"D:\Users\mohem_000\Desktop\Kodak test images";
            var outDir = inDir +  @"\out\";
            var files = Directory.GetFiles(inDir);
            var encBin = enc.EncodeString(@"吾輩は猫である。名前はまだ無い。どこで生れたかとんと見当がつかぬ。何でも薄暗いじめじめした所でニャーニャー泣いていた事だけは記憶している。吾輩はここで始めて人間というものを見た。しかもあとで聞くとそれは書生という人間中で一番獰悪な種族であったそうだ。この書生というのは時々我々を捕えて煮て食うという話である。しかしその当時は何という考もなかったから別段恐しいとも思わなかった。ただ彼の掌に載せられてスーと持ち上げられた時何だかフワフワした感じがあったばかりである。");
            var data = new StegoData(encBin);
            foreach (var file in files)
            {
                Console.WriteLine(file);

                var image = IO.ReadImage(file);
                IO.SaveImage(image, Path.Combine(outDir, Path.GetFileName(file) + @".orig.bmp"), ImageFormat.Bmp);

                var stego = Steganography.Write(image, data);
                IO.SaveImage(stego, Path.Combine(outDir, Path.GetFileName(file) + @".stegoRGB.bmp"), ImageFormat.Bmp);
            }
        }
    }
}