﻿using CLAP;
using CLAP.Validation;
using Sleipnir;
using System;
using System.Drawing.Imaging;
using System.IO;
using System.Text;
using DEC = Sleipnir.ReedSolomon.Decoder;
using ENC = Sleipnir.ReedSolomon.Encoder;

namespace OdinCUI
{
    internal static class Program
    {
        private static int Main(string[] args)
        {
            return Parser.RunConsole<Odin>(args);
        }
    }

    internal class Odin
    {
        [Empty, Help]
        public static void Help(string help)
        {
            Console.WriteLine(help);
        }

        [Verb]
        public static int Encode(
            [Required, FileExists, Aliases("i"), Description("file path of picture.")]
            string input,
            [Required, Aliases("o"), Description("file path of picture.")]
            string output,
            [Aliases("b"), Description("read file in binary mode.")]
            string binaryPath,
            [Aliases("s"), Description("direct encode from argument.")]
            string str,
            [Aliases("t"), Description("read file in text mode (must be UTF-8).")]
            string textPath)
        {
            if (!output.EndsWith(".png"))
            {
                output += ".png";
            }

            var image = IO.ReadImage(input);
            var enc = new ENC();

            if (!string.IsNullOrEmpty(binaryPath))
            {
                if (!File.Exists(binaryPath))
                {
                    Console.WriteLine("input binary is not found.");
                    return -1;
                }

                var binFile = File.ReadAllBytes(binaryPath);
                var encBin = enc.EncodeBytes(binFile);
                var data = new StegoData(encBin);
                image = Steganography.Write(image, data);

                IO.SaveImage(image, output, ImageFormat.Png);
            }
            else if (!string.IsNullOrEmpty(str))
            {
                var encBin = enc.EncodeString(str);
                var data = new StegoData(encBin);
                image = Steganography.Write(image, data);

                IO.SaveImage(image, output, ImageFormat.Png);
            }
            else
            {
                if (!File.Exists(textPath))
                {
                    Console.WriteLine("input text is not found.");
                    return -1;
                }

                var txtFile = File.ReadAllText(textPath, Encoding.UTF8);
                var encBin = enc.EncodeString(txtFile);
                var data = new StegoData(encBin);
                image = Steganography.Write(image, data);

                IO.SaveImage(image, output, ImageFormat.Png);
            }

            return 0;
        }

        [Verb]
        public static int Decode(
            [Required, FileExists, Aliases("i"), Description("file path of picture.")]
            string input,
            [Aliases("t"), Description("read file in text mode.")]
            bool text,
            [Aliases("b"), Description("read file in binary mode.")]
            bool binary,
            [Aliases("o"), Description("file path of extracted data.")]
            string output,
            [Description("output extracted data to stdout (default).")]
            bool stdout)
        {
            if ((!text && !binary) || (text && binary))
            {
                text = true;
            }

            var image = IO.ReadImage(input);
            var stegoData = Steganography.Read(image);
            var dec = new DEC();
            if (text)
            {
                var decText = dec.DecodeToString(stegoData.Data);
                if (string.IsNullOrEmpty(output) || stdout)
                {
                    Console.WriteLine(decText);
                }
                else
                {
                    using (var fs = new FileStream(output, FileMode.Create))
                    using (var sw = new StreamWriter(fs, Encoding.UTF8))
                    {
                        sw.WriteLineAsync(decText).Wait();
                    }
                }
            }
            else
            {
                var decBytes = dec.DecodeToBytes(stegoData.Data);
                if (string.IsNullOrEmpty(output) || stdout)
                {
                    throw new NotSupportedException();
                }
                else
                {
                    using (var fs = new FileStream(output, FileMode.Create))
                    {
                        fs.Write(decBytes, 0, decBytes.Length);
                    }
                }
            }

            return 0;
        }
    }
}