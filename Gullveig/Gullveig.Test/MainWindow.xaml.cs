﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Gullveig.Test
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();

            Loaded += (s, a) =>
            {
                new Action(async () =>
                {
                    await Task.Run(() => System.Threading.Thread.Sleep(3000));
                    for (var i = 3; i > 0; i--)
                    {
                        StatusBar.Text = i.ToString();
                        await Task.Run(() => System.Threading.Thread.Sleep(1000));
                    }
                    
                    StatusBar.Text = "ババーン";
                })();
            };
        }
    }
}
