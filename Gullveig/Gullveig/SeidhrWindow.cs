﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shell;

namespace Gullveig
{
    public class SeidhrWindow : Window
    {
        protected BorderedWindow BorderedWindow { get; private set; }

        public SeidhrWindow()
        {
            Loaded += (sender, args) =>
            {
                StateChanged += EventProxy.InvokeOnStateChanged;
            };
        }

        protected override void OnSourceInitialized(EventArgs e)
        {
            base.OnSourceInitialized(e);

            var chrome = new WindowChrome { CaptionHeight = 0, UseAeroCaptionButtons = false };
            WindowChrome.SetWindowChrome(this, chrome);
        }

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);

            Loaded += (s, a) =>
            {
                // 地の果てまで追い詰めて内部Gridを取得する
                var child = VisualTreeHelper.GetChild(this, 0);
                new Action(() =>
                {
                    var maxSearch = long.MaxValue;
                    var count = 0L;
                    while (count < maxSearch)
                    {
                        try
                        {
                            if (child.GetType() == typeof(BorderedWindow))
                            {
                                BorderedWindow = (BorderedWindow)child;
                                return;
                            }
                            else
                            {
                                child = VisualTreeHelper.GetChild(child, 0);
                            }
                            count++;
                        }
                        catch
                        {
                            return;
                        }
                    }
                })();
            };

            StateChanged += (s, a) =>
            {
                var marginValue = 0;
                if (this.WindowState == WindowState.Maximized)
                {
                    marginValue = 8;
                }
                BorderedWindow.Margin = new Thickness(marginValue);
            };
        }

        protected override void OnActivated(EventArgs e)
        {
            base.OnActivated(e);
            // アタッチしたウィンドウ内の要素すべてに通知してアクセントカラーの適用・非適用を切り替えられるようにする
            EventProxy.InvokeOnActivated(this, e);
        }

        protected override void OnDeactivated(EventArgs e)
        {
            base.OnDeactivated(e);
            EventProxy.InvokeOnDeactivated(this, e);
        }
    }
}