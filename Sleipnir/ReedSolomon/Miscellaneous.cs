﻿using Sleipnir.Helpers;
using System.Collections;
using System.Collections.Generic;

namespace Sleipnir.ReedSolomon
{
    /// <summary>
    /// リード・ソロモン符号に関連するフィールドやメソッドを扱うクラス
    /// </summary>
    public static class Miscellaneous
    {
        /// <summary>
        /// ガロア体を保持するリスト
        /// </summary>
        public static readonly List<BitArray> GaloisField = GenerateGaloisField();

        /// <summary>
        /// ガロア体の生成
        /// </summary>
        /// <returns>生成したガロア体を表すBitArrayのリスト</returns>
        private static List<BitArray> GenerateGaloisField()
        {
            var result = new List<BitArray>(256);
            result.Add(new BitArray((new[] { 0, 0, 0, 0, 0, 0, 0, 1 }).ConvertToBitArrayBoolean()));
            result.Add(new BitArray((new[] { 0, 0, 0, 0, 0, 0, 1, 0 }).ConvertToBitArrayBoolean()));
            result.Add(new BitArray((new[] { 0, 0, 0, 0, 0, 1, 0, 0 }).ConvertToBitArrayBoolean()));
            result.Add(new BitArray((new[] { 0, 0, 0, 0, 1, 0, 0, 0 }).ConvertToBitArrayBoolean()));
            result.Add(new BitArray((new[] { 0, 0, 0, 1, 0, 0, 0, 0 }).ConvertToBitArrayBoolean()));
            result.Add(new BitArray((new[] { 0, 0, 1, 0, 0, 0, 0, 0 }).ConvertToBitArrayBoolean()));
            result.Add(new BitArray((new[] { 0, 1, 0, 0, 0, 0, 0, 0 }).ConvertToBitArrayBoolean()));
            result.Add(new BitArray((new[] { 1, 0, 0, 0, 0, 0, 0, 0 }).ConvertToBitArrayBoolean()));
            result.Add(new BitArray((new[] { 0, 0, 0, 1, 1, 1, 0, 1 }).ConvertToBitArrayBoolean()));

            var a8 = new BitArray(result[8]);

            // a^9 ～ a^254 まで生成する
            for (var i = 8; i < 254; i++)
            {
                var tmp = result[result.Count - 1];

                if (tmp[0])
                {
                    tmp = tmp.ShiftLeft();
                    tmp = tmp.Xor(a8);
                }
                else
                {
                    tmp = tmp.ShiftLeft();
                }

                result.Add(tmp);
            }

            // a^255 (0) を追加する
            result.Add(new BitArray((new[] { 0, 0, 0, 0, 0, 0, 0, 0 }).ConvertToBitArrayBoolean()));

            return result;
        }

        /// <summary>
        /// 生成多項式を生成する
        /// </summary>
        /// <param name="codeLength">符号シンボルの長さ</param>
        /// <param name="parityLength">冗長シンボルの長さ</param>
        /// <returns></returns>
        public static int[] GenerateGenerationPolynomial(int codeLength, int parityLength)
        {
            var result = new int[parityLength + 1];

            var dataLength = codeLength - parityLength;

            result[0] = 2;
            result[1] = 1;

            for (var i = 2; i <= (codeLength - dataLength); i++)
            {
                result[i] = 1;
                for (var j = i - 1; j >= 1; j--)
                {
                    if ((result[j] != 0))
                    {
                        result[j] = result[j - 1] ^ GaloisField[(GaloisField.GetIndex(result[j]) + i) % 255].ConvertToByte();
                    }
                    else
                    {
                        result[j] = result[j - 1];
                    }
                }
                result[0] = GaloisField[(GaloisField.GetIndex(result[0]) + i) % 255].ConvertToByte();
            }

            for (var i = 0; i <= parityLength; i++)
            {
                result[i] = GaloisField.GetIndex(result[i]);
            }

            return result;
        }
    }
}