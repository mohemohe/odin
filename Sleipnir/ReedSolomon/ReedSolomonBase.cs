﻿#region LICENSE

/*             rs.c        */
/* This program is an encoder/decoder for Reed-Solomon codes. Encoding is in
   systematic form, decoding via the Berlekamp iterative algorithm.
   In the present form , the constants mm, nn, tt, and kk=nn-2tt must be
   specified  (the double letters are used simply to avoid clashes with
   other n,k,t used in other programs into which this was incorporated!)
   Also, the irreducible polynomial used to generate GF(2**mm) must also be
   entered -- these can be found in Lin and Costello, and also Clark and Cain.

   The representation of the elements of GF(2**m) is either in index form,
   where the number is the power of the primitive element alpha, which is
   convenient for multiplication (add the powers modulo 2**m-1) or in
   polynomial form, where the bits represent the coefficients of the
   polynomial representation of the number, which is the most convenient form
   for addition.  The two forms are swapped between via lookup tables.
   This leads to fairly messy looking expressions, but unfortunately, there
   is no easy alternative when working with Galois arithmetic.

   The code is not written in the most elegant way, but to the best
   of my knowledge, (no absolute guarantees!), it works.
   However, when including it into a simulation program, you may want to do
   some conversion of global variables (used here because I am lazy!) to
   local variables where appropriate, and passing parameters (eg array
   addresses) to the functions  may be a sensible move to reduce the number
   of global variables and thus decrease the chance of a bug being introduced.

   This program does not handle erasures at present, but should not be hard
   to adapt to do this, as it is just an adjustment to the Berlekamp-Massey
   algorithm. It also does not attempt to decode past the BCH bound -- see
   Blahut "Theory and practice of error control codes" for how to do this.

              Simon Rockliff, University of Adelaide   21/9/89

   26/6/91 Slight modifications to remove a compiler dependent bug which hadn't
           previously surfaced. A few extra comments added for clarity.
           Appears to all work fine, ready for posting to net!

                  Notice
                 --------
   This program may be freely modified and/or given to whoever wants it.
   A condition of such distribution is that the author's contribution be
   acknowledged by his name being left in the comments heading the program,
   however no responsibility is accepted for any financial or other loss which
   may result from some unforseen errors or malfunctioning of the program
   during use.
                                 Simon Rockliff, 26th June 1991
*/

#endregion LICENSE

using Sleipnir.Helpers;
using System;
using System.Linq;

namespace Sleipnir.ReedSolomon
{
    /// <summary>
    /// リード・ソロモン符号を扱うための基底クラス　一部を除いてrs.cより移植
    /// </summary>
    public class ReedSolomonBase
    {
        // 2^8 - 1
        protected const int MaxLength = 255;

        /// <summary>
        /// ReedSolomon符号の初期化
        /// </summary>
        /// <param name="codeLength">符号シンボルの長さ</param>
        /// <param name="parityLength">冗長シンボルの長さ 必ず2以上であること</param>
        public ReedSolomonBase(int codeLength = 26, int parityLength = 7)
        {
            CodeLength = codeLength;
            ParityLength = parityLength;
            DataLength = CodeLength - ParityLength;

            GenerationPolynomial = Miscellaneous.GenerateGenerationPolynomial(codeLength, parityLength);
        }

        /// <summary>
        /// 符号シンボルの長さ
        /// </summary>
        protected int CodeLength { get; }

        /// <summary>
        /// 情報シンボルの長さ
        /// </summary>
        protected int DataLength { get; }

        /// <summary>
        /// 冗長シンボルの長さ
        /// </summary>
        protected int ParityLength { get; }

        /// <summary>
        /// 生成多項式を表す配列
        /// </summary>
        protected int[] GenerationPolynomial { get; }

        /// <summary>
        /// 符号化
        /// </summary>
        /// <param name="data">符号化対象のbyte配列</param>
        /// <returns>符号化済みのbyte配列</returns>
        protected byte[] Encode(byte[] data)
        {
            var result = new byte[CodeLength];

            var parity = Enumerable.Repeat(0, ParityLength).ToList();
            for (var i = (DataLength - 1); i >= 0; i--)
            {
                var xor = Miscellaneous.GaloisField.GetIndex(data[i] ^ parity[ParityLength - 1]);
                if ((xor != -1))
                {
                    for (var j = (ParityLength - 1); j >= 1; j--)
                    {
                        if ((GenerationPolynomial[j] != -1))
                        {
                            parity[j] = parity[j - 1] ^
                                        Miscellaneous.GaloisField[(GenerationPolynomial[j] + xor) % MaxLength]
                                            .ConvertToByte();
                        }
                        else
                        {
                            parity[j] = parity[j - 1];
                        }
                    }
                    parity[0] =
                        Miscellaneous.GaloisField[(GenerationPolynomial[0] + xor) % MaxLength].ConvertToByte();
                }
                else
                {
                    for (var j = (ParityLength - 1); j >= 1; j--)
                    {
                        parity[j] = parity[j - 1];
                    }
                    parity[0] = 0;
                }
            }

            for (var i = 0; i <= (ParityLength - 1); i++)
            {
                result[i] = (byte)parity[i];
            }
            for (var i = 0; i <= (DataLength - 1); i++)
            {
                result[i + ParityLength] = data[i];
            }

            return result;
        }

        /// <summary>
        /// 復号　失敗した場合は例外を吐くので適切に処理する必要がある
        /// </summary>
        /// <param name="data">復号対象のbyte配列</param>
        /// <returns>復号済みのbyte配列</returns>
        protected byte[] Decode(byte[] data)
        {
            var result = new byte[DataLength];

            var foundError = false;

            var x = new int[ParityLength + 2];
            var y = new int[ParityLength + 2];

            var ulu = new int[ParityLength + 2];
            var syndrome = new int[ParityLength + 1];
            var root = new int[ParityLength / 2];
            var position = new int[ParityLength / 2];

            var error = new int[CodeLength];
            var register = new int[(ParityLength / 2) + 1];

            var elp = new int[ParityLength + 2][];
            for (var i = 0; i < elp.Length; i++)
            {
                elp[i] = new int[ParityLength];
            }

            for (var i = 0; i < (CodeLength); i++)
            {
                data[i] = (byte)Miscellaneous.GaloisField.GetIndex(data[i]);
            }

            var count = 0;
            for (var i = 1; i <= ParityLength; i++)
            {
                syndrome[i] = 0;
                for (var j = 0; j < CodeLength; j++)
                {
                    if (data[j] != 255)
                    {
                        syndrome[i] = syndrome[i] ^ Miscellaneous.GaloisField[(data[j] + i * j) % MaxLength].ConvertToByte();
                    }
                }

                foundError = (syndrome[i] != 0);
                syndrome[i] = Miscellaneous.GaloisField.GetIndex(syndrome[i]);
            }

            var check = new Action(() =>
            {
                for (var i = 0; i < CodeLength; i++)
                {
                    if (data[i] != 255)
                    {
                        data[i] = Miscellaneous.GaloisField[data[i]].ConvertToByte();
                    }
                    else
                    {
                        data[i] = 0;
                    }
                }
            });

            if (foundError)
            {
                x[0] = 0;
                x[1] = syndrome[1];
                elp[0][0] = 0;
                elp[1][0] = 1;
                for (var i = 1; i < ParityLength; i++)
                {
                    elp[0][i] = -1;
                    elp[1][i] = 0;
                }
                y[0] = 0;
                y[1] = 0;
                ulu[0] = -1;
                ulu[1] = 0;

                var u = 0;
                while (((u < ParityLength) && (y[u + 1] <= ParityLength / 2)))
                {
                    u++;
                    if ((x[u] == -1))
                    {
                        y[u + 1] = y[u];
                        for (var i = 0; i <= y[u]; i++)
                        {
                            elp[u + 1][i] = elp[u][i];
                            elp[u][i] = Miscellaneous.GaloisField.GetIndex(elp[u][i]);
                        }
                    }
                    else
                    {
                        var q = u - 1;
                        while (((x[q] == -1) && (q > 0)))
                        {
                            q -= 1;
                        }
                        if ((q > 0))
                        {
                            var j = q;
                            while (j > 0)
                            {
                                j -= 1;
                                if (((x[j] != 255) && (ulu[q] < ulu[j])))
                                {
                                    q = j;
                                }
                            }
                        }
                        if ((y[u] > y[q] + u - q))
                        {
                            y[u + 1] = y[u];
                        }
                        else
                        {
                            y[u + 1] = y[q] + u - q;
                        }
                        for (var i = 0; i < ParityLength; i++)
                        {
                            elp[u + 1][i] = 0;
                        }
                        for (var i = 0; i <= y[q]; i++)
                        {
                            if ((elp[q][i] != 255))
                            {
                                elp[u + 1][i + u - q] =
                                    Miscellaneous.GaloisField[(x[u] + MaxLength - x[q] + elp[q][i]) % MaxLength]
                                        .ConvertToByte();
                            }
                        }
                        for (var i = 0; i <= y[u]; i++)
                        {
                            elp[u + 1][i] = elp[u + 1][i] ^ elp[u][i];
                            elp[u][i] = Miscellaneous.GaloisField.GetIndex(elp[u][i]);
                        }
                    }
                    ulu[u + 1] = u - y[u + 1];
                    if (u < ParityLength)
                    {
                        if ((syndrome[u + 1] != 255))
                        {
                            x[u + 1] = Miscellaneous.GaloisField[syndrome[u + 1]].ConvertToByte();
                        }
                        else
                        {
                            x[u + 1] = 0;
                        }
                        for (var i = 1; i <= y[u + 1]; i++)
                        {
                            if (((syndrome[u + 1 - i] != 255) && (elp[u + 1][i] != 0)))
                            {
                                x[u + 1] = x[u + 1] ^
                                              Miscellaneous.GaloisField[
                                                  (syndrome[u + 1 - i] +
                                                   Miscellaneous.GaloisField.GetIndex(elp[u + 1][i])) %
                                                  MaxLength].ConvertToByte();
                            }
                        }
                        x[u + 1] = Miscellaneous.GaloisField.GetIndex(x[u + 1]);
                    }
                }

                u++;

                if (y[u] <= Convert.ToInt64(ParityLength / 2))
                {
                    for (var i = 0; i <= y[u]; i++)
                    {
                        elp[u][i] = Miscellaneous.GaloisField.GetIndex(elp[u][i]);
                    }
                    for (var i = 1; i <= y[u]; i++)
                    {
                        register[i] = elp[u][i];
                    }
                    for (var i = 1; i <= MaxLength; i++)
                    {
                        var q = 1;
                        for (var j = 1; j <= y[u]; j++)
                        {
                            if (register[j] != 255)
                            {
                                register[j] = (register[j] + j) % MaxLength;
                                q = q ^ Miscellaneous.GaloisField[register[j]].ConvertToByte();
                            }
                        }
                        if (q == 0)
                        {
                            root[count] = i;
                            position[count] = MaxLength - i;
                            count++;
                        }
                    }
                    if (count == y[u])
                    {
                        var z = new int[(ParityLength / 2) + 1];

                        for (var i = 1; i <= y[u]; i++)
                        {
                            if (((syndrome[i] != 255) && (elp[u][i] != 255)))
                            {
                                z[i] = Miscellaneous.GaloisField[syndrome[i]].ConvertToByte() ^
                                       Miscellaneous.GaloisField[elp[u][i]].ConvertToByte();
                            }
                            else if (((syndrome[i] != 255) && (elp[u][i] == -1)))
                            {
                                z[i] = Miscellaneous.GaloisField[syndrome[i]].ConvertToByte();
                            }
                            else if (((syndrome[i] == -1) && (elp[u][i] != 255)))
                            {
                                z[i] = Miscellaneous.GaloisField[elp[u][i]].ConvertToByte();
                            }
                            else
                            {
                                z[i] = 0;
                            }
                            for (var j = 1; j <= (i - 1); j++)
                            {
                                if (((syndrome[j] != 255) && (elp[u][i - j] != 255)))
                                {
                                    z[i] = z[i] ^
                                           Miscellaneous.GaloisField[(elp[u][i - j] + syndrome[j]) % MaxLength]
                                               .ConvertToByte();
                                }
                            }
                            z[i] = Miscellaneous.GaloisField.GetIndex(z[i]);
                        }

                        for (var i = 0; i < CodeLength; i++)
                        {
                            error[i] = 0;
                            if (data[i] != 255)
                            {
                                data[i] = Miscellaneous.GaloisField[data[i]].ConvertToByte();
                            }
                            else
                            {
                                data[i] = 0;
                            }
                        }
                        for (var i = 0; i < y[u]; i++)
                        {
                            error[position[i]] = 1;
                            for (var j = 1; j <= y[u]; j++)
                            {
                                if (z[j] != 255)
                                {
                                    error[position[i]] = error[position[i]] ^
                                                         Miscellaneous.GaloisField[(z[j] + j * root[i]) % MaxLength]
                                                             .ConvertToByte();
                                }
                            }
                            if (error[position[i]] != 0)
                            {
                                error[position[i]] = Miscellaneous.GaloisField.GetIndex(error[position[i]]);
                                var q = 0;
                                for (var j = 0; j <= (y[u] - 1); j++)
                                {
                                    if (j != i)
                                    {
                                        q = q +
                                            Miscellaneous.GaloisField.GetIndex(1 ^
                                                                               Miscellaneous.GaloisField[
                                                                                   (position[j] + root[i]) % MaxLength]
                                                                                   .ConvertToByte());
                                    }
                                }
                                q = q % MaxLength;
                                error[position[i]] =
                                    Miscellaneous.GaloisField[(error[position[i]] - q + MaxLength) % MaxLength]
                                        .ConvertToByte();
                                data[position[i]] = (byte)(data[position[i]] ^ error[position[i]]);
                            }
                        }
                    }
                    else
                    {
                        check();
                    }
                }
                else
                {
                    check();
                }
            }
            else
            {
                check();
            }

            for (var i = ParityLength; i < CodeLength; i++)
            {
                result[i - ParityLength] = data[i];
            }

            return result;
        }
    }
}