﻿using System.Collections.Generic;
using System.Text;

namespace Sleipnir.ReedSolomon
{
    /// <summary>
    /// リード・ソロモン符号の符号化を行うクラス
    /// </summary>
    public sealed class Encoder : ReedSolomonBase
    {
        /// <summary>
        /// ReedSolomonエンコーダーの初期化
        /// </summary>
        /// <param name="codeLength">符号シンボルの長さ</param>
        /// <param name="parityLength">冗長シンボルの長さ 必ず2以上であること</param>
        public Encoder(int codeLength = 26, int parityLength = 7) : base(codeLength, parityLength)
        {
        }

        /// <summary>
        /// 文字列を符号化する
        /// </summary>
        /// <param name="word">符号化する文字列</param>
        /// <returns>符号化後のbyte配列</returns>
        public byte[] EncodeString(string word)
        {
            return EncodeBytes(Encoding.UTF8.GetBytes(word));
        }

        /// <summary>
        /// byte配列を符号化する
        /// </summary>
        /// <param name="words">符号化するbyte配列</param>
        /// <returns>符号化後のbyte配列</returns>
        public byte[] EncodeBytes(byte[] words)
        {
            var codesList = new List<byte>();

            var wordsList = new List<byte>(words);
            while (wordsList.Count % DataLength != 0)
            {
                wordsList.Add(0);
            }

            for (var i = 0; i < wordsList.Count; i += DataLength)
            {
                var word = wordsList.GetRange(i, DataLength).ToArray();
                var code = Encode(word);
                codesList.AddRange(code);
            }

            return codesList.ToArray();
        }
    }
}