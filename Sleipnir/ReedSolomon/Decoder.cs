﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Sleipnir.ReedSolomon
{
    /// <summary>
    /// リード・ソロモン符号の復号を行うクラス
    /// </summary>
    public class Decoder : ReedSolomonBase
    {
        /// <summary>
        /// ReedSolomonデコーダーの初期化
        /// </summary>
        /// <param name="codeLength">符号シンボルの長さ</param>
        /// <param name="parityLength">冗長シンボルの長さ 必ず2以上であること</param>
        public Decoder(int codeLength = 26, int parityLength = 7) : base(codeLength, parityLength)
        {
        }

        /// <summary>
        /// 文字列に復号する
        /// </summary>
        /// <param name="codes">符号のbyte配列</param>
        /// <returns>復号後の文字列</returns>
        public string DecodeToString(byte[] codes)
        {
            return Encoding.UTF8.GetString(DecodeToBytes(codes));
        }

        /// <summary>
        /// byte配列に復号する
        /// </summary>
        /// <param name="codes">符号のbyte配列</param>
        /// <returns>復号後のbyte配列</returns>
        public byte[] DecodeToBytes(byte[] codes)
        {
            var wordsList = new List<byte>();

            var codesList = new List<byte>(codes);
            var count = codes.Length;
            for (var i = 0; i < CodeLength - (count % CodeLength); i++)
            {
                codesList.Add(0);
            }

            for (var i = 0; i < codesList.Count; i += CodeLength)
            {
                var code = codesList.GetRange(i, CodeLength);
                try
                {
                    var word = Decode(code.ToArray());
                    wordsList.AddRange(word);
                }
                catch
                {
                    var raw = code.GetRange(ParityLength, DataLength);
                    wordsList.AddRange(raw);
                    Debug.WriteLine("error!");
                }
            }

            return wordsList.ToArray();
        }
    }
}