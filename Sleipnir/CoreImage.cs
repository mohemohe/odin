﻿using System.Drawing;
using System.Drawing.Imaging;

namespace Sleipnir
{
    /// <summary>
    /// 本ライブラリにおいて画像を便利に扱うためのクラス
    /// </summary>
    public class CoreImage
    {
        /// <summary>
        /// 読み込み元のファイルパス
        /// </summary>
        public string FilePath { get; set; }

        /// <summary>
        /// デコード済みのBitmapオブジェクト
        /// </summary>
        public Bitmap Bitmap { get; set; }

        /// <summary>
        /// 画像の横幅　Bitmapオブジェクトがnullである場合は0を返す
        /// </summary>
        public int Width { get { return Bitmap != null ? Bitmap.Width : 0; } }

        /// <summary>
        /// 画像の高さ　Bitmapオブジェクトがnullである場合は0を返す
        /// </summary>
        public int Height { get { return Bitmap != null ? Bitmap.Height : 0; } }

        /// <summary>
        /// 画像のフォーマット　Bitmapオブジェクトがnullである場合はnullを返す
        /// </summary>
        public ImageFormat Format { get { return Bitmap != null ? Bitmap.RawFormat : null; } }

        /// <summary>
        /// 画像のファイルパスを引き継いで自身を複製する
        /// </summary>
        /// <returns>複製後のCoreImage</returns>
        public CoreImage Clone()
        {
            return new CoreImage
            {
                FilePath = this.FilePath,
                Bitmap = (Bitmap)this.Bitmap.Clone()
            };
        }
    }
}