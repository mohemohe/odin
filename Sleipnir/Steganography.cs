﻿using Sleipnir.Helpers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Threading.Tasks;
using Mohemohe.Helpers;

namespace Sleipnir
{
    /// <summary>
    /// ステガノグラフィを行うクラス
    /// </summary>
    public static class Steganography
    {
        /// <summary>
        /// 画像へステガノグラフィを行う
        /// </summary>
        /// <param name="image">埋め込み先のCoreImageオブジェクト</param>
        /// <param name="data">埋め込むStegoDataオブジェクト</param>
        /// <param name="blockSize">冗長化のブロックサイズ</param>
        /// <returns>埋め込み後のCoreImageオブジェクト</returns>
        public static unsafe CoreImage Write(CoreImage image, StegoData data, int blockSize = 8)
        {
            var rand = new Xorshift();

            var decodedBmp = image.Bitmap.Clone(new Rectangle(Point.Empty, new Size(image.Width, image.Height)), PixelFormat.Format32bppArgb); 
            var bmp = decodedBmp.LockBits(new Rectangle(Point.Empty, new Size(image.Width, image.Height)),
                ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);
            var bmpPtr = (byte*)(void*)bmp.Scan0;
            var getCappedValue = new Func<byte, int, byte>((a, b) =>
            {
                var fluctuation = rand.Next(2) - 1;

                if (a == 25 && b == 1)
                {
                    return Convert.ToByte(247 + fluctuation);
                }
                else
                {
                    switch (b)
                    {
                        case 0:
                            return Convert.ToByte(a * 10 + fluctuation + 2);

                        case 1:
                            return Convert.ToByte(a * 10 + fluctuation + 7);

                        default:
                            throw new NotImplementedException();
                    }
                }
            });

            new Action(() =>
            {
                var dataList = data.ToLinearIntListSlow();
                dataList.Add(-1);
                var linearBitArray = dataList.ToArray();
                fixed (int* dataPtrZero = linearBitArray)
                {
                    var dataPtr = dataPtrZero;

                    for (var y = 0; y < bmp.Height - blockSize; y += blockSize)
                    {
                        var bmpPtrLineStartPos = bmpPtr;

                        for (var x = 0; x < bmp.Width - blockSize; x += blockSize)
                        {
                            var bmpPtrBlockStartPos = bmpPtr;

                            for (var by = y; by < y + blockSize; by++)
                            {
                                for (var bx = x; bx < x + blockSize; bx++)
                                {
                                    var r = (byte)(*(bmpPtr + 2) / 10.0);
                                    byte rVal;
                                    byte gVal;
                                    var b = (byte)(*bmpPtr / 10.0);
                                    byte bVal;

                                    switch (*dataPtr)
                                    {
                                        case 0:
                                        case 1:
                                            rVal = getCappedValue(r, *dataPtr);
                                            gVal = *(bmpPtr + 1);
                                            bVal = getCappedValue(b, *dataPtr);
                                            break;

                                        default:
                                            return;
                                    }
                                    *(bmpPtr + 2) = rVal;
                                    *(bmpPtr + 1) = gVal;
                                    *(bmpPtr) = bVal;

                                    // Note: [B, G, R, A] | next B
                                    bmpPtr += 4;
                                }

                                // Note: next line -> first pixel -> B
                                bmpPtr += 4 * (image.Width - blockSize);
                            }
                            bmpPtr = bmpPtrBlockStartPos + (blockSize * 4);
                            dataPtr++;

                            if (*dataPtr == -1)
                            {
                                return;
                            }
                        }
                        bmpPtr = bmpPtrLineStartPos + (image.Width * 4 * blockSize);
                    }
                }
            })();
            decodedBmp.UnlockBits(bmp);
            image.Bitmap = decodedBmp.Clone(new Rectangle(Point.Empty, new Size(image.Width, image.Height)), PixelFormat.Format32bppArgb);
            decodedBmp.Dispose();
            return image;
        }

        /// <summary>
        /// 画像へステガノグラフィを非同期に行う
        /// </summary>
        /// <param name="image">埋め込み先のCoreImageオブジェクト</param>
        /// <param name="data">埋め込むStegoDataオブジェクト</param>
        /// <param name="blockSize">冗長化のブロックサイズ</param>
        /// <returns>埋め込み後のCoreImageオブジェクト</returns>
        public static async Task<CoreImage> WriteAsync(CoreImage image, StegoData data, int blockSize = 8)
        {
            return await Awaitable<CoreImage>.Run(() => Write(image, data, blockSize));
        }

        /// <summary>
        /// 画像から埋め込まれた秘密文を読み出す
        /// </summary>
        /// <param name="image">埋め込み先のCoreImageオブジェクト</param>
        /// <param name="blockSize">冗長化のブロックサイズ</param>
        /// <returns>読み出したStegoDataオブジェクト</returns>
        public static unsafe StegoData Read(CoreImage image, int blockSize = 8)
        {
            var result = new StegoData();

            var bmp = image.Bitmap.LockBits(new Rectangle(Point.Empty, new Size(image.Width, image.Height)),
                ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);
            var bmpPtr = (byte*)bmp.Scan0;

            var x = 0;
            var y = 0;
            var proxy = false;
            var _bmpPtrLineStartPos = bmpPtr;

            var checkRB = new Func<int, int, int, int, bool>((r, b, min, max) =>
            {
                return ((r >= min && r <= max) && (b >= min && b <= max));
            });

            var header = new List<int>();
            new Action(delegate
            {
                for (y = 0; y < bmp.Height - blockSize; y += blockSize)
                {
                    var bmpPtrLineStartPos = bmpPtr;

                    for (x = 0; x < bmp.Width - blockSize; x += blockSize)
                    {
                        var bmpPtrBlockStartPos = bmpPtr;
                        var valueList = new List<int>(blockSize * blockSize);

                        for (var by = y; @by < y + blockSize; @by++)
                        {
                            for (var bx = x; bx < x + blockSize; bx++)
                            {
                                var r = *(bmpPtr + 2) % 10;
                                var b = *bmpPtr % 10;

                                if (checkRB(r, b, 0, 4))
                                {
                                    valueList.Add(0);
                                }
                                else if (checkRB(r, b, 5, 9))
                                {
                                    valueList.Add(1);
                                }
                                else
                                {
                                    valueList.Add(-1);
                                }

                                // Note: [B, G, R, A] | next B
                                bmpPtr += 4;
                            }

                            // Note: next line -> first pixel -> B
                            bmpPtr += 4 * (image.Width - blockSize);
                        }
                        header.Add(valueList.Count(a => a == 0) > valueList.Count(a => a == 1) ? 0 : 1);
                        bmpPtr = bmpPtrBlockStartPos + (blockSize * 4);

                        if (header.Count == result.HeaderLength)
                        {
                            if (x == bmp.Width - blockSize)
                            {
                                _bmpPtrLineStartPos = bmpPtr = bmpPtrLineStartPos + (image.Width * 4 * blockSize);
                            }
                            proxy = true;
                            return;
                        }
                    }
                    _bmpPtrLineStartPos = bmpPtr = bmpPtrLineStartPos + (image.Width * 4 * blockSize);
                }
            })();
            result.SetHeader(header.ConvertToBytesList().ToArray());

            bmpPtr -= 4;
            x += blockSize;

            var payloadData = new List<int>();
            new Action(() =>
            {
                for (; y < bmp.Height - blockSize; y += blockSize)
                {
                    var bmpPtrLineStartPos = _bmpPtrLineStartPos;

                    x = (proxy) ? x : 0;
                    proxy = false;
                    for (; x < bmp.Width - blockSize; x += blockSize)
                    {
                        var valueList = new List<int>(blockSize * blockSize);
                        var bmpPtrBlockStartPos = bmpPtr;

                        for (var by = y; by < y + blockSize; by++)
                        {
                            for (var bx = x; bx < x + blockSize; bx++)
                            {
                                var r = *(bmpPtr + 2) % 10;
                                //var g = *(bmpPtr + 1)%10;
                                var b = *bmpPtr % 10;

                                if (checkRB(r, b, 0, 4))
                                {
                                    valueList.Add(0);
                                }
                                else if (checkRB(r, b, 5, 9))
                                {
                                    valueList.Add(1);
                                }
                                else
                                {
                                    valueList.Add(-1);
                                }

                                // Note: [B, G, R, A] | next B
                                bmpPtr += 4;
                            }

                            // Note: next line -> first pixel -> B
                            bmpPtr += 4 * (image.Width - blockSize);
                        }
                        payloadData.Add(valueList.Count(a => a == 0) > valueList.Count(a => a == 1) ? 0 : 1);

                        bmpPtr = bmpPtrBlockStartPos + (blockSize * 4);

                        if (payloadData.Count >= result.PayloadDataLength)
                        {
                            return;
                        }
                    }
                    _bmpPtrLineStartPos = bmpPtr = bmpPtrLineStartPos + (image.Width * 4 * blockSize);
                }
            })();
            result.SetPayloadData(payloadData.ConvertToBytesList().ToArray());

            image.Bitmap.UnlockBits(bmp);

            return result;
        }

        /// <summary>
        /// 画像から埋め込まれた秘密文を非同期に読み出す
        /// </summary>
        /// <param name="image">埋め込み先のCoreImageオブジェクト</param>
        /// <param name="blockSize">冗長化のブロックサイズ</param>
        /// <returns>読み出したStegoDataオブジェクト</returns>
        public static async Task<StegoData> ReadAsync(CoreImage image, int blockSize = 8)
        {
            return await Awaitable<StegoData>.Run(() => Read(image, blockSize));
        }

        /// <summary>
        /// 物理層の冗長化のブロックサイズを計算する
        /// </summary>
        /// <param name="image">埋め込み先のCoreImageオブジェクト</param>
        /// <returns>ブロックサイズ</returns>
        public static unsafe int GetPhysicalBlockSize(CoreImage image)
        {
            var result = -1;

            var bmp = image.Bitmap.LockBits(new Rectangle(Point.Empty, new Size(image.Width, image.Height)),
                ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);
            var bmpPtr = (byte*)(void*)bmp.Scan0;

            for (var i = 1; i <= image.Width; i++)
            {
                var r = *(bmpPtr + 2) % 10.0;
                var b = *bmpPtr % 10.0;
                if ((r >= 0 && r <= 4) && (b >= 0 && b <= 4))
                {
                    result = i;
                }
                else
                {
                    break;
                }

                // Note: [B, G, R, A] | next B
                bmpPtr += 4;
            }

            image.Bitmap.UnlockBits(bmp);
            return result;
        }

        /// <summary>
        /// 物理層の冗長化のブロックサイズを非同期に計算する
        /// </summary>
        /// <param name="image">埋め込み先のCoreImageオブジェクト</param>
        /// <returns>ブロックサイズ</returns>
        public static async Task<int> GetPhysicalBlockSizeAsync(CoreImage image)
        {
            return await Awaitable<int>.Run(() => GetPhysicalBlockSize(image));
        }
    }
}