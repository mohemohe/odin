﻿using Sleipnir.Helpers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Sleipnir
{
    /// <summary>
    /// ステガノグラフィのデータを便利に扱うためのクラス
    /// </summary>
    public class StegoData : StegoDataStructure
    {
        /// <summary>
        /// エンディアンを表す
        /// </summary>
        private enum Endian
        {
            LittleEndian,
            BigEndian,
            Unknown
        }

        private Endian _endian;
        private HEADER _header;
        private PAYLOAD _payload;

        #region Header

        /// <summary>
        /// 埋め込みメソッドのバージョン
        /// </summary>
        public byte Method
        {
            get { return _header.Method; }
        }

        /// <summary>
        /// データ長
        /// </summary>
        public int DataLength
        {
            get { return _header.DataLength; }
        }

        #endregion Header

        #region Payload

        /// <summary>
        /// データ
        /// </summary>
        public byte[] Data
        {
            get
            {
                return _payload.Data;
            }
            private set
            {
                _payload.Data = value;
                _header.DataLength = value.Length * 8;
            }
        }

        #endregion Payload

        /// <summary>
        /// 全体のバイト長
        /// </summary>
        public long Size
        {
            get { return HeaderSize + PayloadSize; }
        }

        /// <summary>
        /// 全体のビット長
        /// </summary>
        public long Length
        {
            get { return HeaderLength + PayloadLength; }
        }

        /// <summary>
        /// ヘッダーのバイト長
        /// </summary>
        public int HeaderSize
        {
            get { return Marshal.SizeOf(_header); }
        }

        /// <summary>
        /// ヘッダーのビット長
        /// </summary>
        public int HeaderLength
        {
            get { return HeaderSize * 8; }
        }

        /// <summary>
        /// ペイロードのバイト長
        /// </summary>
        public int PayloadSize
        {
            get { return sizeof(byte) * _payload.Data.Length + Marshal.SizeOf(_payload.Pattern); }
        }

        /// <summary>
        /// ペイロードのビット長
        /// </summary>
        public int PayloadLength
        {
            get { return PayloadSize * 8; }
        }

        /// <summary>
        /// ペイロードのデータのバイト長
        /// </summary>
        public int PayloadDataSize
        {
            get { return sizeof(byte) * _payload.Data.Length; }
        }

        /// <summary>
        /// ペイロードのデータのビット長
        /// </summary>
        public int PayloadDataLength
        {
            get { return (_payload.Data != null) ? PayloadDataSize * 8 : _header.DataLength; }
        }

        /// <summary>
        /// 空のStegoDataオブジェクトを作成する
        /// </summary>
        public StegoData()
        {
            CheckEndian();
            InitializeHeader();
            InitializePayload();
        }

        /// <summary>
        /// ペイロードのデータを指定してStegoDataオブジェクトを作成する
        /// </summary>
        /// <param name="data"></param>
        public StegoData(byte[] data)
        {
            CheckEndian();
            InitializeHeader();
            InitializePayload();
            Data = data;
        }

        /// <summary>
        /// ヘッダーを設定する
        /// </summary>
        /// <param name="header">設定するヘッダーのbyte配列</param>
        public unsafe void SetHeader(byte[] header)
        {
            fixed (byte* headerPtr = header)
            {
                _header = *(HEADER*)headerPtr;
            }
        }

        /// <summary>
        /// ペイロードのデータを設定する
        /// </summary>
        /// <param name="payloadData">設定するペイロードのbyte配列</param>
        public void SetPayloadData(byte[] payloadData)
        {
            _payload.Data = payloadData;
        }

        /// <summary>
        /// エンディアンをチェックする
        /// </summary>
        private void CheckEndian()
        {
            var one = new BitArray(new byte[] { 1 }).ToIntList();
            if (one[0] == 1)
            {
                _endian = Endian.LittleEndian;
            }
            // リトルエンディアン以外は今のところ未対応
            else if (one[one.Count - 1] == 1)
            {
                //_endian = Endian.BigEndian;
                throw new NotSupportedException(@"Your CPU is not supported.");
            }
            else
            {
                //_endian = Endian.Unknown;
                throw new NotSupportedException(@"Your CPU is not supported.");
            }
        }

        /// <summary>
        /// ヘッダーの生成をする
        /// </summary>
        private void InitializeHeader()
        {
            //var pattern = new BitArray((new[] { 0, 1, 0, 1, 1, 1, 0, 1 }).ConvertToBitArrayBoolean()).ConvertToByte();
            var pattern = _endian == Endian.LittleEndian ? (byte)0xBA : (byte)0x5D;

            _header = new HEADER
            {
                Pattern = pattern,
                Method = 0x01
            };
        }

        /// <summary>
        /// ペイロードの生成をする
        /// </summary>
        private void InitializePayload()
        {
            _payload = new PAYLOAD
            {
                Pattern = unchecked((int)uint.MaxValue)
            };
        }

        /// <summary>
        /// [非推奨] ペイロードのデータを0と1で構成されたビットを表すリストにする
        /// </summary>
        /// <returns></returns>
        [Obsolete]
        public unsafe List<int> ToLinearIntList()
        {
            var result = new List<int>();

            var headerBytes = new byte[HeaderSize];
            fixed (byte* headerBytesPtr = headerBytes)
            {
                *(HEADER*)headerBytesPtr = _header;
            }
            result.AddRange(headerBytes.ConvertToBitList());

            var payloadBytes = new byte[PayloadSize];
            var payloadBytesPtr = Marshal.AllocHGlobal(PayloadSize);
            Marshal.StructureToPtr(_header, payloadBytesPtr, false);
            Marshal.Copy(payloadBytesPtr, payloadBytes, 0, PayloadSize);
            Marshal.FreeHGlobal(payloadBytesPtr);
            result.AddRange(payloadBytes.ConvertToBitList());

            return result;
        }

        /// <summary>
        /// ペイロードのデータを0と1で構成されたビットを表すリストにする
        /// </summary>
        /// <returns></returns>
        public List<int> ToLinearIntListSlow()
        {
            var result = new List<int>();

            // header
            {
                result.AddRange(_header.Pattern.ConvertToBitList());
                result.AddRange(_header.Method.ConvertToBitList());
                result.AddRange(_header.DataLength.ConvertToBitList());
                result.AddRange((new byte[6]).ConvertToBitList());
            }

            // payload
            {
                result.AddRange(_payload.Data.ConvertToBitList());
                result.AddRange(_payload.Pattern.ConvertToBitList());
            }

            return result;
        }
    }
}