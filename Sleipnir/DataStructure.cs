﻿using System.Runtime.InteropServices;

namespace Sleipnir
{
    public class DataStructure
    {
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        protected struct HEADER
        {
            // byte
            internal byte Pattern;

            // byte
            internal byte Method;

            // int
            internal int DataLength;

            // 8*byte
            // 将来の拡張用　基本的に読み飛ばしてよい
            internal long Reserved;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        protected struct PAYLOAD
        {
            internal byte[] Data;

            // 0xFFFFFFFFである必要がある
            internal int Pattern;
        }
    }
}
