﻿namespace Sleipnir
{
    /// <summary>
    /// System.Random と同じような感じで使える Xorshift
    /// </summary>
    internal class Xorshift
    {
        private ulong _x = 123456789, _y = 362436069, _z = 521288629, _w;

        /// <summary>
        /// マジックナンバーによる固定のシード値を使用して Xorshift インスタンスを初期化する
        /// </summary>
        /// <param name="seed">シード値</param>
        public Xorshift(ulong seed = 88675123)
        {
            _w = seed;

            // 適度に読み飛ばす
            for (var i = 0; i < 32; i++) Xor128();
        }

        /// <summary>
        /// 0 以上の乱数を返す
        /// </summary>
        /// <returns></returns>
        public ulong NextUlong()
        {
            return Xor128();
        }

        /// <summary>
        /// 指定した最大値より小さい 0 以上の乱数を返す
        /// </summary>
        /// <param name="max">最大値</param>
        /// <returns></returns>
        public ulong NextUlong(ulong max)
        {
            var raw = Xor128();
            if (max == 0) return 0;
            return raw % max;
        }

        /// <summary>
        /// 指定した範囲内の乱数を返す
        /// </summary>
        /// <param name="min">最小値</param>
        /// <param name="max">最大値</param>
        /// <returns></returns>
        public ulong NextUlong(ulong min, ulong max)
        {
            var raw = Xor128();
            if (max - min == 0) return min;
            return raw % (max - min);
        }

        /// <summary>
        /// 0 以上の乱数を返す
        /// </summary>
        /// <returns></returns>
        public int Next()
        {
            var raw = Xor128();
            return (int)(raw % int.MaxValue);
        }

        /// <summary>
        /// 指定した最大値より小さい 0 以上の乱数を返す
        /// </summary>
        /// <param name="max">最大値</param>
        /// <returns></returns>
        public int Next(int max)
        {
            if (max == 0) return 0;
            var raw = Xor128();
            return (int)(raw % (uint)max);
        }

        /// <summary>
        /// 指定した範囲内の乱数を返す
        /// </summary>
        /// <param name="min">最小値</param>
        /// <param name="max">最大値</param>
        /// <returns></returns>
        public int Next(int min, int max)
        {
            if (max == 0) return 0;
            var raw = Xor128();
            return (int)(raw % (uint)(max - min));
        }

        /// <summary>
        /// 0.0 と 1.0 の間の乱数を返す
        /// </summary>
        /// <returns></returns>
        public double NextDouble()
        {
            var raw = Xor128();
            return (double)raw / ulong.MaxValue;
        }

        private ulong Xor128()
        {
            ulong t = _x ^ (_x << 11);
            _x = _y; _y = _z; _z = _w;
            return _w = (_w ^ (_w >> 19)) ^ (t ^ (t >> 8));
        }
    }
}