﻿using Sleipnir.Helpers;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Threading.Tasks;

namespace Sleipnir
{
    /// <summary>
    /// 画像の入出力を便利に行うためのクラス
    /// </summary>
    public static class IO
    {
        /// <summary>
        /// ファイルから画像を読み込んでCoreImageオブジェクトを生成する
        /// </summary>
        /// <param name="filePath">読み込み元のファイルパス</param>
        /// <returns>生成したCoreImageオブジェクト</returns>
        public static CoreImage ReadImage(string filePath)
        {
            CoreImage image;
            using (var bitmap = new Bitmap(filePath))
            {
                image = new CoreImage { FilePath = filePath, Bitmap = (Bitmap)bitmap.Clone() };
            }

            return image;
        }

        /// <summary>
        /// ファイルから非同期に画像を読み込んでCoreImageオブジェクトを生成する
        /// </summary>
        /// <param name="filePath">読み込み元のファイルパス</param>
        /// <returns>生成したCoreImageオブジェクト</returns>
        public static async Task<CoreImage> ReadImageAsync(string filePath)
        {
            return await Awaitable<CoreImage>.Run(() => ReadImage(filePath));
        }

        /// <summary>
        /// CoreImageオブジェクトの画像を保存する
        /// </summary>
        /// <param name="image">保存するCoreImageオブジェクト</param>
        /// <param name="filePath">保存先のファイルパス</param>
        /// <param name="fileFormat">画像フォーマット</param>
        public static void SaveImage(CoreImage image, string filePath = null, ImageFormat fileFormat = null)
        {
            if (filePath == null)
            {
                var ext = Path.GetExtension(image.FilePath);
                filePath = image.FilePath.Remove(image.FilePath.Length - ext.Length) + @"." + DateTime.Now.Ticks + ext;
            }
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
            image.Bitmap.Save(
                filePath,
                fileFormat ?? image.Format
                );
        }

        /// <summary>
        /// CoreImageオブジェクトの画像を非同期に保存する
        /// </summary>
        /// <param name="image">保存するCoreImageオブジェクト</param>
        /// <param name="filePath">保存先のファイルパス</param>
        /// <param name="fileFormat">画像フォーマット</param>
        public static async Task SaveImageAsync(CoreImage image, string filePath = null, ImageFormat fileFormat = null)
        {
            await Task.Run(() => SaveImage(image, filePath, fileFormat));
        }
    }
}