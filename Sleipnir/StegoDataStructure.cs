﻿using System.Runtime.InteropServices;

namespace Sleipnir
{
    /// <summary>
    /// ステゴデータのデータ構造を定義するクラス
    /// </summary>
    public class StegoDataStructure
    {
        /// <summary>
        /// ヘッダーのデータ構造
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        protected struct HEADER
        {
            // 合計96bytes

            // byte
            internal byte Pattern;

            // byte
            internal byte Method;

            // int
            internal int DataLength;

            // 6*byte
            // 将来の拡張用　基本的に読み飛ばしてよい
            internal byte Reserved1;

            internal byte Reserved2;
            internal byte Reserved3;
            internal byte Reserved4;
            internal byte Reserved5;
            internal byte Reserved6;
        }

        /// <summary>
        /// ペイロードのデータ構造
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        protected struct PAYLOAD
        {
            internal byte[] Data;

            // 0xFFFFFFFFである必要がある
            internal int Pattern;
        }
    }
}