﻿using Sleipnir.ReedSolomon;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Sleipnir.Helpers
{
    /// <summary>
    /// BitArrayを便利に扱うための拡張メソッド群
    /// </summary>
    public static class BitArrayExtension
    {
        /// <summary>
        /// いずれががTrueであるかどうか
        /// </summary>
        /// <param name="bArray"></param>
        /// <returns></returns>
        public static bool Any(this BitArray bArray)
        {
            var result = false;
            for (var i = 0; i < bArray.Count; i++)
            {
                if (bArray[i])
                {
                    result = true;
                }
            }

            return result;
        }

        /// <summary>
        /// すべてがTrueであるかどうか
        /// </summary>
        /// <param name="bArray"></param>
        /// <returns></returns>
        public static bool All(this BitArray bArray)
        {
            var result = true;
            var origin = bArray[0];
            for (var i = 1; i < bArray.Count; i++)
            {
                if (origin != bArray[i])
                {
                    result = false;
                }
            }

            return result;
        }

        /// <summary>
        /// 指定した場所のビットを反転する
        /// </summary>
        /// <param name="bArray"></param>
        /// <param name="position"></param>
        /// <returns></returns>
        public static bool Flip(this BitArray bArray, int position)
        {
            if (bArray.Count > position)
            {
                bArray.Set(position, !bArray[position]);
                return true;
            }
            return false;
        }

        /// <summary>
        /// ビット列を左にシフトする
        /// </summary>
        /// <param name="bArray"></param>
        /// <param name="repeat">シフトする回数</param>
        /// <returns></returns>
        public static BitArray ShiftLeft(this BitArray bArray, int repeat = 1)
        {
            var t = new BitArray(bArray.Count);

            for (var i = 0; i < repeat; i++)
            {
                for (var j = 0; j < bArray.Count - 1; j++)
                {
                    t[j] = bArray[j + 1];
                }
                t[bArray.Count - 1] = false;
            }

            return t;
        }

        /// <summary>
        /// ビット列を右にシフトする
        /// </summary>
        /// <param name="bArray"></param>
        /// <param name="repeat">シフトする回数</param>
        /// <returns></returns>
        public static BitArray ShiftRight(this BitArray bArray, int repeat = 1)
        {
            var t = new BitArray(bArray.Count);

            for (var i = 0; i < repeat; i++)
            {
                for (var j = 1; j < bArray.Count; j++)
                {
                    t[j] = bArray[j - 1];
                }
                t[0] = false;
            }

            return t;
        }

        /// <summary>
        /// ビット列の順番を反転する
        /// </summary>
        /// <param name="bArray"></param>
        /// <returns></returns>
        public static BitArray Reverce(this BitArray bArray)
        {
            var result = new BitArray(bArray.Length);
            for (var i = 0; i < bArray.Count; i++)
            {
                result[bArray.Length - 1 - i] = bArray[i];
            }

            return result;
        }

        /// <summary>
        /// ビット列同士を加算する
        /// </summary>
        /// <param name="bArray1"></param>
        /// <param name="bArray2"></param>
        /// <returns></returns>
        public static BitArray Add(this BitArray bArray1, BitArray bArray2)
        {
            return bArray1.Xor(bArray2);
        }

        /// <summary>
        /// ビット列同士を乗算する
        /// </summary>
        /// <param name="bArray1"></param>
        /// <param name="bArray2"></param>
        /// <returns></returns>
        public static BitArray Multiply(this BitArray bArray1, BitArray bArray2)
        {
            if (!bArray1[0] && bArray1.All() || !bArray2[0] && bArray2.All())
            {
                return new BitArray(bArray1.Length);
            }

            var gf = Miscellaneous.GaloisField;

            var ba1Index = bArray1.GetGFIndex();
            var ba2Index = bArray2.GetGFIndex();
            var index = ba1Index + ba2Index;

            while (index > gf.Count)
            {
                index -= gf.Count;
            }

            return gf[index];
        }

        /// <summary>
        /// ビット列同士を冪演算する
        /// </summary>
        /// <param name="bArray"></param>
        /// <param name="x"></param>
        /// <returns></returns>
        public static BitArray Pow(this BitArray bArray, int x)
        {
            var result = new BitArray(bArray.Length);

            if (x > 0)
            {
                result = bArray;
                for (var i = 0; i < x; i++)
                {
                    result = result.Multiply(result);
                }
            }

            return result;
        }

        /// <summary>
        /// ビット列と一致するガロア体のindexを取得数
        /// </summary>
        /// <param name="bArray"></param>
        /// <returns></returns>
        public static int GetGFIndex(this BitArray bArray)
        {
            var gf = Miscellaneous.GaloisField;

            var index = -1;
            for (var i = 0; i < gf.Count; i++)
            {
                if (bArray.IsEqual(gf[i]))
                {
                    index = i;
                }
            }

            if (index == -1)
            {
                throw new NotImplementedException();
            }

            return index;
        }

        /// <summary>
        /// ビット列を指定した桁数に揃える
        /// </summary>
        /// <param name="bArray"></param>
        /// <param name="length">桁数</param>
        /// <returns></returns>
        public static BitArray Normalize(this BitArray bArray, int length = 8)
        {
            if (bArray.Count == length)
            {
                return bArray;
            }

            var tmp = new List<bool>();
            new List<char>(bArray.ConvertToString().PadLeft(length, '0').ToCharArray()).ForEach(x => tmp.Add(x == '1'));
            tmp.Reverse();
            return new BitArray(tmp.ToArray());
        }

        /// <summary>
        /// 2つのビット列が論理的に等しいかどうか　桁数は自動的に揃える
        /// </summary>
        /// <param name="bArray1"></param>
        /// <param name="bArray2"></param>
        /// <returns></returns>
        public static bool IsEqual(this BitArray bArray1, BitArray bArray2)
        {
            var result = true;

            var bN1 = bArray1.Normalize();
            var bN2 = bArray2.Normalize();

            for (var i = 0; i < bN1.Count; i++)
            {
                if (bN1[i] != bN2[i])
                {
                    result = false;
                    break;
                }
            }

            return result;
        }

        /// <summary>
        /// BitArrayで表現されるビット列と等価なbyteに変える
        /// </summary>
        /// <param name="bArray"></param>
        /// <returns></returns>
        public static byte ConvertToByte(this BitArray bArray)
        {
            var bytes = new byte[1];
            bArray.CopyTo(bytes, 0);
            return bytes[0];
        }

        /// <summary>
        /// ビット列を文字列として表現する
        /// </summary>
        /// <param name="bArray"></param>
        /// <returns></returns>
        public static string ConvertToString(this BitArray bArray)
        {
            var sb = new StringBuilder();

            for (var i = 0; i < bArray.Count; i++)
            {
                var c = bArray[i] ? '1' : '0';
                sb.Append(c);
            }

            return sb.ToString();
        }

        /// <summary>
        /// ビット列を0と1で構成されたリストにする
        /// </summary>
        /// <param name="bArray"></param>
        /// <returns></returns>
        public static List<int> ToIntList(this BitArray bArray)
        {
            var list = new List<int>();

            for (var i = 0; i < bArray.Count; i++)
            {
                var value = bArray[i] ? 1 : 0;
                list.Add(value);
            }

            return list;
        }

        /// <summary>
        /// ビット列のリストを0と1で構成されたリストのリストにする
        /// </summary>
        /// <param name="bArray"></param>
        /// <returns></returns>
        public static List<List<int>> ToIntList(this List<BitArray> bArray)
        {
            var list = new List<List<int>>();

            bArray.ForEach(x => list.Add(x.ToIntList()));
            return list;
        }

        /// <summary>
        /// ビット列のリストを0と1で構成された1次元のリストにする
        /// </summary>
        /// <param name="bArray"></param>
        /// <returns></returns>
        public static List<int> ToLinearIntList(this List<BitArray> bArray)
        {
            var list = new List<int>();

            bArray.ForEach(x => list.AddRange(x.ToIntList()));
            return list;
        }

        /// <summary>
        /// ビット列のリストから指定した数と等しいビット列のindexを得る
        /// </summary>
        /// <param name="bArray"></param>
        /// <param name="value">indexを取得したい数</param>
        /// <returns></returns>
        public static int GetIndex(this List<BitArray> bArray, int value)
        {
            var result = -1;

            for (var i = 0; i < bArray.Count; i++)
            {
                if (bArray[i].ConvertToByte() == value)
                {
                    result = i;
                    break;
                }
            }

            if (result == 255)
            {
                // 定義されていない
                result = -1;
            }

            return result;
        }
    }
}