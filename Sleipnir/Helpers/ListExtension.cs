﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

namespace Sleipnir.Helpers
{
    /// <summary>
    /// リストを便利に扱うための拡張メソッド群
    /// </summary>
    public static class ListExtension
    {
        /// <summary>
        /// 0と1で構成されたビットを表すintリストを得る
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public static List<int> ConvertToBitList(this IConvertible number)
        {
            var result = new List<int>();

            var length = Marshal.SizeOf(number.GetType()) * 8;
            var bits = Convert.ToInt32(number);

            for (var i = 0; i < length; i++)
            {
                var bit = (bits & 1);
                bits = bits >> 1;

                result.Add(bit);
            }

            return result;
        }

        /// <summary>
        /// 0と1で構成されたビットを表すintリストを得る
        /// </summary>
        /// <param name="array"></param>
        /// <returns></returns>
        public static List<int> ConvertToBitList(this byte[] array)
        {
            var result = new List<int>();

            array.ToList().ForEach(x => result.AddRange(x.ConvertToBitList()));

            return result;
        }

        /// <summary>
        /// リストを指定した数のリストのリストに分割する
        /// </summary>
        /// <param name="list"></param>
        /// <param name="splits"></param>
        /// <returns></returns>
        public static List<List<int>> SplitElements(this List<int> list, int splits = 8)
        {
            var result = new List<List<int>>();

            var count = list.Count;
            for (var i = 0; i < splits - (count % splits); i++)
            {
                list.Add(0);
            }

            for (var i = 0; i < list.Count; i += splits)
            {
                result.Add(list.GetRange(i, splits));
            }

            return result;
        }

        /// <summary>
        /// intリストを分割して等価なbyteリストを得る
        /// </summary>
        /// <param name="array"></param>
        /// <returns></returns>
        public static List<byte> ConvertToBytesList(this List<int> array)
        {
            var result = new List<byte>();

            var elements = array.SplitElements();
            foreach (var element in elements)
            {
                result.Add(new BitArray(element.ToArray().ConvertToBitArrayBoolean()).ConvertToByte());
            }

            return result;
        }
    }
}