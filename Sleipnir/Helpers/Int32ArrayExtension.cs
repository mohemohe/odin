﻿namespace Sleipnir.Helpers
{
    /// <summary>
    /// int配列を便利に扱うための拡張メソッド群
    /// </summary>
    public static class Int32ArrayExtension
    {
        /// <summary>
        /// 配列と等価なbool配列を作成する
        /// </summary>
        /// <param name="bitField"></param>
        /// <returns></returns>
        public static bool[] ConvertToBitArrayBoolean(this int[] bitField)
        {
            var result = new bool[bitField.Length];
            for (var i = 0; i < bitField.Length; i++)
            {
                result[i] = (bitField[i] == 1);
            }

            return result;
        }
    }
}