﻿using System;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

namespace Sleipnir.Helpers
{
    /// <summary>
    /// 任意の型を返す処理を非同期に行うための補助クラス
    /// </summary>
    /// <typeparam name="T"></typeparam>
    class Awaitable<T>
    {
        readonly TaskCompletionSource<T> _tcs;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public Awaitable()
        {
            _tcs = new TaskCompletionSource<T>();
        }

        /// <summary>
        /// 指定の処理を非同期に実行する
        /// </summary>
        /// <param name="action">非同期に動かしたい処理</param>
        private void DoWorkAsync(Func<T> action)
        {
            ThreadPool.QueueUserWorkItem(_ =>
            {
                try { _tcs.SetResult(action()); }
                catch (Exception ex) { _tcs.SetException(ex); }
            });
        }

        /// <summary>
        /// 対応するAwaiterを取得する
        /// </summary>
        /// <returns>対応するAwaiter</returns>
        public TaskAwaiter<T> GetAwaiter()
        {
            return _tcs.Task.GetAwaiter();
        }

        /// <summary>
        /// 非同期処理の実行をラップする
        /// </summary>
        /// <param name="action">非同期に動かしたい処理</param>
        /// <returns>await可能なオブジェクト</returns>
        public static Awaitable<T> Run(Func<T> action)
        {
            var awaitable = new Awaitable<T>();
            awaitable.DoWorkAsync(action);
            return awaitable;
        }
    }
}