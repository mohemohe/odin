﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media.Imaging;
using CoreImage = Sleipnir.CoreImage;
using SIZE = System.Windows.Size;

namespace Odin.Helpers
{
    public static class CoreImageHelper
    {
        public static SIZE LogicalSize(this CoreImage image, int blockSize)
        {
            return new SIZE(image.Width / blockSize, image.Height / blockSize);
        }

        public static BitmapSource BitmapSource(this CoreImage image)
        {
            return image.Bitmap.BitmapSource();
        }

        // NOTE: これはBitmapHelperに移動したほうがいい？
        public static BitmapSource BitmapSource(this Bitmap image)
        {
            var hBitmap = image.GetHbitmap();
            BitmapSource source;

            try
            {
                source = Imaging.CreateBitmapSourceFromHBitmap(
                    hBitmap,
                    IntPtr.Zero,
                    Int32Rect.Empty,
                    BitmapSizeOptions.FromEmptyOptions());
            }
            catch (Exception e)
            {
                Debug.WriteLine(DateTime.Now);
                Debug.WriteLine(e.StackTrace);
                source = null;
            }
            finally
            {
                // ここの破棄が妙に遅い
                Win32Helper.DeleteObject(hBitmap);
            }

            return source;
        }
    }
}