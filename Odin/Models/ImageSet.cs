﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Livet;
using Sleipnir;

namespace Odin.Models
{
    public class ImageSet : NotificationObject
    {
        /*
         * NotificationObjectはプロパティ変更通知の仕組みを実装したオブジェクトです。
         */

        public StegoData StegoData { get; set; }
        public CoreImage CoreImage { get; set; }

        public BitmapSource BitmapSource
        {
            get
            {
                if (CoreImage != null && CoreImage.Bitmap != null)
                {
                    return CoreImage.Bitmap
                }
            }
        }

        public ImageSet()
        {
            
        }

        public ImageSet(string ImagePath)
        {
            new Action( async() =>
            {
                
            })();
        }
    }
}
