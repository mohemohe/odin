﻿using System.Windows;
using System.Windows.Input;
using System.Windows.Interactivity;

namespace Odin.Behaviors
{
    internal class DragDropBehavior : Behavior<FrameworkElement>
    {
        public static readonly DependencyProperty DragDropProperty =
            DependencyProperty.Register("Command", typeof(ICommand), typeof(DragDropBehavior), new UIPropertyMetadata(null));

        protected override void OnAttached()
        {
            base.OnAttached();
            this.AssociatedObject.PreviewDragOver += OnPreviewDragOver;
            this.AssociatedObject.Drop += OnDrop;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            this.AssociatedObject.PreviewDragOver -= OnPreviewDragOver;
            this.AssociatedObject.Drop -= OnDrop;
        }

        public ICommand Command
        {
            get { return (ICommand)GetValue(DragDropProperty); }
            set { SetValue(DragDropProperty, value); }
        }

        private void OnPreviewDragOver(object sender, System.Windows.DragEventArgs e)
        {
            e.Effects = e.Data.GetDataPresent(DataFormats.FileDrop, true) ?
                DragDropEffects.Copy :
                DragDropEffects.None;
            e.Handled = true;
        }

        private void OnDrop(object sender, System.Windows.DragEventArgs e)
        {
            if (Command == null || !Command.CanExecute(e))
            {
                return;
            }
            Command.Execute((string[])(e.Data.GetData(DataFormats.FileDrop)));
        }
    }
}