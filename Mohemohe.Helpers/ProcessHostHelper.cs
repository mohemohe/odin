﻿/*
 * Copyright (c) 2015 Jun Kasamatsu (mohemohe)
 * Released under the MIT license
 * http://opensource.org/licenses/mit-license.php
 */

using System;
using System.Data;
using System.Diagnostics;
using System.Threading;

namespace Mohemohe.Helpers
{
    // 子プロセスをホスティングするクラス
    // IDisposable インターフェースを継承しているので
    //
    // static void Main(string[] args)
    // {
    //     using(var ph = new ProcessHost())
    //     {
    //         ph.Process.OutputDataReceived += (sender, e) => StdInRouter(e.Data);
    //     }
    // }
    //
    // static void StdInRouter(string str)
    // {
    //     var value = int.Parse(str);
    //
    //     switch (value)
    //     {
    //         ...
    //     }
    // }
    //
    // などして使うとよい
    // ただし標準出力の受けとりは非同期なので、注意する必要がある
    public class ProcessHostHelper : IDisposable
    {
        public Process Process { get; private set; }
        private Thread StdInThread { get; set; }
        private bool IsForceKeepTask { get; set; }

        public ProcessHostHelper(string processName)
        {
            if (string.IsNullOrEmpty(processName))
            {
                throw new NoNullAllowedException();
            }

            var psi = new ProcessStartInfo
            {
                FileName = processName,

                // ウィンドウを生成せずに裏でひっそりと動かす
                CreateNoWindow = true,
                UseShellExecute = false,

                // この3つで標準あれこれをリダイレクトする
                RedirectStandardError = true,
                RedirectStandardInput = true,
                RedirectStandardOutput = true,
            };

            Process = Process.Start(psi);
            // コンソールアプリの場合はコメントアウトすると標準入力が取れる
            //Task.Run(() => ReadLineLoop());
            Process.EnableRaisingEvents = true;
            Process.BeginOutputReadLine();
            Process.BeginErrorReadLine();

            Process.Exited += (sender, e) => Dispose();

            // そのまま使うことはないと思うので一応コメントアウト
            /*
            Process.OutputDataReceived += (sender, e) =>
            {
                if (e.Data != null)
                {
                    // ここで標準出力が取れる(e.Data)
                    Console.WriteLine(e.Data);
                }
            };
            Process.ErrorDataReceived += (sender, e) =>
            {
                if (e.Data != null)
                {
                    // ここで標準エラーが取れる(e.Data)
                    // 多分使わない
                    Console.WriteLine(e.Data);
                }
            };
            */

            TaskKeeper();
        }

        // マルチスレッドで動かしたときにスレッドが死なないようにループさせているだけ
        private void TaskKeeper()
        {
            IsForceKeepTask = true;
            while (IsForceKeepTask)
            {
                Thread.Sleep(1);
            }
        }

        // コンソール上から直接標準入力に出力するループ
        // 不要なら上の Task.Run()～ と一緒に消しても良い
        private void ReadLineLoop()
        {
            StdInThread = Thread.CurrentThread;

            while (true)
            {
                var stdin = Console.ReadLine();
                WriteStdIn(stdin);
            }
        }

        // 標準入力に文字列を注入するメソッド
        public void InjectionStdin(string stdin)
        {
            WriteStdIn(stdin);
        }

        private void WriteStdIn(string command)
        {
            Process.StandardInput.WriteLine(command);
        }

        // ループの停止と参照開放
        public void Dispose()
        {
            StdInThread?.Abort();
            if (Process != null)
            {
                Process.Dispose();
                Process = null;
            }
            IsForceKeepTask = false;
        }
    }
}