﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media.Imaging;
using Point = System.Drawing.Point;
using Size = System.Drawing.Size;

namespace Mohemohe.Helpers
{
    public static class BitmapHelper
    {
        public static BitmapSource ToBitmapSource(this Bitmap image)
        {
            var hBitmap = image.GetHbitmap();
            BitmapSource source;

            try
            {
                source = Imaging.CreateBitmapSourceFromHBitmap(
                    hBitmap,
                    IntPtr.Zero,
                    Int32Rect.Empty,
                    BitmapSizeOptions.FromEmptyOptions());
            }
            catch (Exception e)
            {
                Debug.WriteLine(DateTime.Now);
                Debug.WriteLine(e.StackTrace);
                source = null;
            }
            finally
            {
                // ここの破棄が妙に遅い
                Win32Helper.DeleteObject(hBitmap);
            }

            return source;
        }

        public static Bitmap ToBitmap(this BitmapSource image)
        {
            var imageSize = new Size((int)image.Width, (int)image.Height);
            var bitmap = new Bitmap(imageSize.Width, imageSize.Height, PixelFormat.Format32bppArgb);
            var bitmapData = bitmap.LockBits(new Rectangle(Point.Empty, imageSize), ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);
            image.CopyPixels(Int32Rect.Empty, bitmapData.Scan0, bitmapData.Width * bitmapData.Stride, bitmapData.Stride);
            bitmap.UnlockBits(bitmapData);
            return bitmap;
        }
    }
}