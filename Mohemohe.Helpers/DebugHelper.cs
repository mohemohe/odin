﻿/*
 * Copyright (c) 2013 Jun Kasamatsu (mohemohe)
 * Released under the MIT license
 * http://opensource.org/licenses/mit-license.php
 */

using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace Mohemohe.Helpers
{
    public static class DebugHelper
    {
        [Conditional("DEBUG")]
        [DllImport("Kernel32")]
        private static extern void AllocConsole();

        [Conditional("DEBUG")]
        [DllImport("Kernel32")]
        private static extern void FreeConsole();

        [Conditional("DEBUG")]
        public static void Initialize()
        {
            var tryCount = 0;
            while (tryCount < 5)
            {
                try
                {
                    AllocConsole();
                    break;
                }
                catch (AccessViolationException)
                {
                    tryCount++;
                }
            }

            var writer = new TextWriterTraceListener(Console.Out);
            Debug.Listeners.Add(writer);
            Console.Title = @"Debug Log";
        }

        [Conditional("DEBUG")]
        public static void Dispose()
        {
            FreeConsole();
        }

        [Conditional("DEBUG")]
        public static void Write(dynamic value)
        {
            Console.Write(value.ToString());
        }

        [Conditional("DEBUG")]
        public static void Write(string format, object arg0)
        {
            Console.Write(format, arg0);
        }

        [Conditional("DEBUG")]
        public static void Write(string format, object arg0, object arg1)
        {
            Console.Write(format, arg0, arg1);
        }

        [Conditional("DEBUG")]
        public static void Write(string format, object arg0, object arg1, object arg2)
        {
            Console.Write(format, arg0, arg1, arg2);
        }

        [Conditional("DEBUG")]
        public static void Write(string format, object arg0, object arg1, object arg2, object arg3, __arglist)
        {
            var argList = new ArgIterator(__arglist);
            var args = new object[argList.GetRemainingCount() + 4];
            args[0] = arg0;
            args[1] = arg1;
            args[2] = arg2;
            args[3] = arg3;
            for (var i = 4; argList.GetRemainingCount() > 0; i++)
            {
                var arg = argList.GetNextArg();
                args[i] = TypedReference.ToObject(arg);
            }

            Console.Write(format, args);
        }

        [Conditional("DEBUG")]
        public static void WriteLine(dynamic value)
        {
            Console.WriteLine(value.ToString());
        }

        [Conditional("DEBUG")]
        public static void WriteLine(string format, object arg0)
        {
            Console.WriteLine(format, arg0);
        }

        [Conditional("DEBUG")]
        public static void WriteLine(string format, object arg0, object arg1)
        {
            Console.WriteLine(format, arg0, arg1);
        }

        [Conditional("DEBUG")]
        public static void WriteLine(string format, object arg0, object arg1, object arg2)
        {
            Console.WriteLine(format, arg0, arg1, arg2);
        }

        [Conditional("DEBUG")]
        public static void WriteLine(string format, object arg0, object arg1, object arg2, object arg3, __arglist)
        {
            var argList = new ArgIterator(__arglist);
            var args = new object[argList.GetRemainingCount() + 4];
            args[0] = arg0;
            args[1] = arg1;
            args[2] = arg2;
            args[3] = arg3;
            for (var i = 4; argList.GetRemainingCount() > 0; i++)
            {
                var arg = argList.GetNextArg();
                args[i] = TypedReference.ToObject(arg);
            }

            Console.WriteLine(format, args);
        }

        [Conditional("DEBUG")]
        public static void SetForegroundColor(ConsoleColor color)
        {
            Console.ForegroundColor = color;
        }

        [Conditional("DEBUG")]
        public static void ResetConsoleColor()
        {
            Console.ResetColor();
        }
    }
}